all: clean build

build: cv.tex
	mkdir -p work
	xelatex -interaction=nonstopmode -halt-on-error -output-directory work cv.tex

clean:
	rm -rf work
